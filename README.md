# hello-jest

## 执行`npm test`
### 1. 没有test文件时
```js
testMatch: **/__tests__/**/*.[jt]s?(x), **/?(*.)+(spec|test).[tj]s?(x) - 0 matches
testPathIgnorePatterns: \\node_modules\\ - 3 matches
testRegex:  - 0 matches
```

1. jest会根据文件名字自动查找当前目录下所有unit test文件；
所以test文件在哪不重要
2. 会忽略`node_modeule`目录；

### 2. 没有测试用例(test)
概念
- `test suite`
- `test`: 测试用例
- `matcher`: 匹配器
诊断实际值，
一个`test`里可以有多个匹配器，只有当所有匹配器通过才算是`test`通过。

## 配置

## 作用域
- 文件
- describe

# Issues:
1. 怎么书写测试用例？
2. 测试文件里的变量`test`, `expect`为何可以不用引入？
3. `not.toBe`为啥不是`notToBe`？背后的哲学？

