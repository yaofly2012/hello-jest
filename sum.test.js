const sum = require('./sum')

test('add 1 + 2 to equal 3', () => {
    expect(sum(1, 2)).toBe(3)
})

test('2 + 2 = 4', () => {
    expect(2 + 2).toBe(4)
})

test('null', () => {
    const n = null;
    expect(n).toBeNull();
    expect(n).toBeDefined();
    expect(n).not.toBeUndefined();
    expect(n).not.toBeTruthy();
    expect(n).toBeFalsy();
});

function throwError(a) {
    if(a === 0) {
        throw new Error('Invalid Param')
    }
}

test('Exception', () => {
    expect(() => {
        throwError(0)
    }).toThrow(Error)
})